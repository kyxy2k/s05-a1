<%@ page language="java" 
	contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"
    import="java.text.SimpleDateFormat"
    import="java.util.Date"
    import="java.util.Locale"
	import="java.util.TimeZone"
    %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Java Server Pages</title>
</head>
<body>
	<!-- JSP allows integration of HTML tags with java syntax -->
	
	<!-- Prints a message in the console -->
	<% System.out.println("Hello from JSP"); %>
	
	<!-- Creates a variable currentDateTime -->
	<%
		SimpleDateFormat dateTime = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
		TimeZone timeZone1 = TimeZone.getTimeZone("Asia/Manila");
		dateTime.setTimeZone(timeZone1);
		Date manila = new Date();
		String currentManilaDateTime = dateTime.format(manila);
		TimeZone timeZone2 = TimeZone.getTimeZone("Japan");
		dateTime.setTimeZone(timeZone2);
		Date japan = new Date();
		String currentJapanDateTime = dateTime.format(japan);
		TimeZone timeZone3 = TimeZone.getTimeZone("Europe/Berlin");
		dateTime.setTimeZone(timeZone3);
		Date germany = new Date();
		String currentGermanyDateTime = dateTime.format(germany);
	%>
	<h1>Our Date and Time now is...</h1>
	<ul>
		<li>Manila: <%= currentManilaDateTime %></li>
		<li>Japan: <%= currentJapanDateTime %></li>
		<li>Germany: <%= currentGermanyDateTime %></li>
	</ul>
	<!-- JSP Declaration -->
	<!-- Allows declaration one or more variables and methods -->
	
	<%!
		private int initVar = 0;
		private int serviceVar = 0;
		private int destroyVar = 0;
	%>
	
	<!-- JSP Method Declaration -->
	<%!
		public void jspInit(){
			initVar++;
			System.out.println("jspInit(): init"+initVar);
		}
		public void jspDestroy(){
			destroyVar++;
			System.out.println("jspDestroy(): destroy"+destroyVar);
		}
	%>
	<!-- JSP Scriplets -->
	<!-- Allow any java language statement, variable, method declaration and expression -->
	<%
		serviceVar++;
		System.out.println("jspService(): service"+serviceVar);
		String content1 = "content1 : " + initVar;
		String content2 = "content2 : " + serviceVar;
		String content3 = "content3 : " + destroyVar;
	%>
	<!-- JSP Expression -->
	<!-- Code placed within the JSP expression tag is written to the output stream of the response -->
	<!-- out.println is no longer required to print values of variables/methods -->
	<h1>JSP</h1>
	<p><%= content1 %></p>
	<p><%= content2 %></p>
	<p><%= content3 %></p>
